{
  const {
    html,
  } = Polymer;
  /**
    `<cells-alberto-login>` Description.

    Example:

    ```html
    <cells-alberto-login></cells-alberto-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-alberto-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsAlbertoLogin extends Polymer.Element {

    static get is() {
      return 'cells-alberto-login';
      
    }
    static get properties() {
      return {
        acambiado: {
          type: Boolean,
          value: false,
          notify: true
        },
        name: {
          type: String,
          value: ""
        },
        password: {
          type: String,
          value: ""
        }
      };
    }
    login(){
      if(this.name === 'Alberto' && this.password === '123'){
        this.dispatchEvent(new CustomEvent('login-correcto', {detail:this.name}))
      }else{
        alert("Datos incorrectos");
      }
    }
    static get template() {
      return html `
      <style include="cells-alberto-login-styles cells-alberto-login-shared-styles"></style>
      <slot></slot>
        <div id="login">
            <h2>Login</h2>
            <input type="text" value="{{name::input}}" id="user" class="form">
            <input type="password" value="{{password::input}}" id="pass" class="form">
            <input type="submit" value="enviar" id="enviar" on-click="login">
        </div>
      `;
    }
  }

  customElements.define(CellsAlbertoLogin.is, CellsAlbertoLogin);
}